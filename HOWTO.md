# MERCADINHO

O projeto é um  sistema de estoque de supermercado no qual é possível gerenciar os produtos e fabricantes.

## Como utilizar

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Pré-requisitos
Para executar a aplicação, você precisará do Tomcat 8, Banco de dados Mysql e JUnit. Segue para uma melhor compreensão:

```
Tomcat 8;
Eclipse IDE;
Banco de dados Mysql;
JUnit 5;
maven.
```

### Instalando

Primeiro, clone o repositório remoto criando um repositório local.

```
git clone https://gitlab.com/AthosVinicius/processo-seletivo.git
```

Utilizando o Eclipse, importe o projeto.

Atualize as dependências com Maven
```
Alt+F5
```
Nas configurações do projeto (Java Build Path), adicione as pastas **test** e **WebContent** para que passam ser encontradas pelo JUnit.

No Mysql crie o Banco de dados com o nome de ``` mercado ```


Iniciar o servidor Tomcat e acessar o endereço local:
http://localhost:8080/mercado


## Realizando os testes

No Eclipse, adicione a biblioteca da JUnit 5 e rode as classes de teste  ```MercadoFabricanteServiceTest``` e ```MercadoProdutoServiceTest```.

Na Classe **MercadoFabricanteServiceTest** ajuste o paramêtro **ID_FABRICANTE_TEST** com um ID existente para os testes de atualização e exclusão.

```
private static final int ID_FABRICANTE_TEST = 1;
```

Na Classe **MercadoProdutoServiceTest** ajuste o paramêtro **ID_FABRICANTE_TEST** com um ID existente para os testes de atualização e inserção.

Ainda na Classe **MercadoProdutoServiceTest** ajuste o paramêtro **ID_PRODUTO_TEST** com um ID existente para os testes de atualização e exclusão.
```
private static final int ID_PRODUTO_TEST = 1;
private static final int ID_FABRICANTE_TEST = 1;
```

## Tecnologias utilizadas

* [Hibernate](http://hibernate.org/) - framework Utilizado
* [Jersey](https://jersey.github.io/) - Serviços Web RESTful em Java
* [Mysql](https://www.mysql.com/) - Banco de dados Usado
* [Toncat](http://tomcat.apache.org/) - Servidor web Java
* [JUnit5](https://junit.org/junit5/) - Framework de Testes
* [Maven](https://maven.apache.org/) - Gerenciador de dependências
* [GIT](https://git-scm.com/) - sistema de controle de versões

## Autores
* **Atos Vinicius** - [Github](https://github.com/AthosVinicius)

## Licença

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
