var app = new Vue({
	el:"#app",
    data: {
        Fabricante_form: {
        	id: null,
        	nome: ''
        },
        listaFabricantes: [],
        listaFabricantesHeader: [
			{sortable: false, key: "nome", label:"Nome"}
		]
    },
    created: function(){
        let vm =  this;
        vm.buscaFabricantes();
    },
    methods:{
    	buscaFabricantes: function(){
			const vm = this;
			axios.get("/mercado/rs/fabricantes")
			.then(response => {
				vm.listaFabricantes = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		edita: function(Fabricante){
			const vm = this;
			vm.errors = [];
			vm.Fabricante_form = JSON.parse(JSON.stringify(Fabricante));

			$('#savebtn').css('display','none');
			$('#updatebtn').css('display','inline');
			$('#modal_new').modal('show');
		},
		altera: function(){
			const vm = this;
			
			axios.put("/mercado/rs/fabricantes/"+vm.Fabricante_form.id, vm.Fabricante_form).then(function(response){
		        vm.buscaFabricantes();
			}).catch(function (error) {
				console.log("Erro interno", "Não foi listar natureza de serviços"+ error);
			}).finally(function() {
				vm.limparForm();
			});
		},

		criar: function(){
			const vm = this;

			vm.errors = [];
			vm.limparForm();
			
			$('#updatebtn').css('display','none');
			$('#savebtn').css('display','inline');
			$('#modal_new').modal('show');
		},
		salvar: function(){
			const vm = this;
			axios.post("/mercado/rs/fabricantes/", { nome: vm.Fabricante_form.nome}).then(function(response){
		        vm.buscaFabricantes();
			}).catch(function (error) {
				console.log("Erro interno", "Não foi listar natureza de serviços"+ error);
			}).finally(function() {
				vm.limparForm();
			});
		},
		remove: function(id){
			const vm = this;
			axios.delete("/mercado/rs/fabricantes/"+id).then(function(response){
		        vm.buscaFabricantes();
			}).catch(function (error) {
				console.log("Erro interno", "Não foi listar natureza de serviços"+ error);
			}).finally(function() {
			});
		},
		limparForm: function(){
			const vm = this;
			vm.Fabricante_form.id = null;
			vm.Fabricante_form.nome = '';
    		$('#modal_new').modal('hide');
		},
	    buscarEmTabela: function() {

	    	  var input, filter, table, tr, td, i, txtValue;
	    	  input = $("#txt_consulta");
	    	  filter = input.val();
	    	  table = $("#result_list");
	    	  tr = $(table).find("tbody tr");

	    	  for (i = 0; i < tr.length; i++) {
	    	    td = $(tr[i]).find("td eq:0");
	    	    if (td) {
	    	      txtValue = $(tr[i]).text();
	    	      if (txtValue.indexOf(filter) > -1) {
	    	    	  $(tr[i]).css('display',"");
	    	        $(tr[i]).addClass('d-flex');
	    	      } else {
	    	    	  $(tr[i]).css('display',"none");
	    	        $(tr[i]).removeClass('d-flex');
	    	      }
	    	    } 
	    	  }
	    	}
    }
});