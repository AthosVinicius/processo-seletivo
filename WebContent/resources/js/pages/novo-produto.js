var app = new Vue({
	el:"#app",
    data: {
        errors: [],
        Produto_form:{
    		id: null,
    		nome: '',
    		fabricante: {
    			id: null
    		},
    		volume: '',
    		unidade: '',
    		estoque: ''
    		
    	},
    	listaFabricantes: [],
        listaProdutos: [],
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		]
    },
    created: function(){
        let vm =  this;
        vm.buscaProdutos();
        vm.buscaFabricantes();
    },
    methods:{
    	
    	buscaFabricantes: function(){
			const vm = this;
			axios.get("/mercado/rs/fabricantes")
			.then(response => {
				vm.listaFabricantes = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		
    	buscaProdutos: function(){
			const vm = this;
			axios.get("/mercado/rs/produtos")
			.then(response => {
				vm.listaProdutos = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		
		edita: function(Produto){
			const vm = this;
			vm.errors = [];
			vm.Produto_form = JSON.parse(JSON.stringify(Produto));

			$('#savebtn').css('display','none');
			$('#updatebtn').css('display','inline');
			$('#modal_new').modal('show');
		},
		
		altera: function(){
			const vm = this;
			
			axios.put("/mercado/rs/produtos/"+vm.Produto_form.id, vm.Produto_form).then(function(response){
		        vm.buscaProdutos();
			}).catch(function (error) {
				console.log("Erro interno", "Não foi listar natureza de serviços"+ error);
			}).finally(function() {
				vm.limparForm();
			});
		},

		
		criar: function(){
			const vm = this;

			vm.errors = [];
			vm.limparForm();
			
			$('#updatebtn').css('display','none');
			$('#savebtn').css('display','inline');
			$('#modal_new').modal('show');
		},
		
		salvar: function(){

			const vm = this;
			
			// validação dos dados
			if(vm.checkForm(vm.Produto_form))
				return;
			
			axios.post("/mercado/rs/produtos/", vm.Produto_form).then(function(response){
		         vm.buscaProdutos();
			}).catch(function (error) {
				console.log("Erro interno", "Não foi listar natureza de serviços"+ error);
			}).finally(function() {
				vm.limparForm();
			});
		},
		
		remove: function(id){
			const vm = this;
			axios.delete("/mercado/rs/produtos/"+id).then(function(response){
		        vm.buscaProdutos();
			}).catch(function (error) {
				console.log("Erro interno", "Não foi listar natureza de serviços"+ error);
			}).finally(function() {
			});
		},
		
		checkForm: function (data) {

			const vm = this;
			vm.errors = [];

		      if (!data.fabricante.id) {
		    	  vm.errors.push('O Fabricante é obrigatório.');
		      }
		      if (!data.nome) {
		    	  vm.errors.push('O nome é obrigatório.');
		      }
		      if (!data.estoque) {
		    	  vm.errors.push('A quantidade no estoque é obrigatória.');
		      }
		      //if (!data.unidade) {
		      //	vm.errors.push('O campo Unidade deve ser numérico.');
		      //}
		      if (isNaN(data.volume)) {
		    	  vm.errors.push('O campo Volume deve ser numérico.');
		      }
		      if (isNaN(data.estoque)) {
		    	  vm.errors.push('O campo Estoque deve ser numérico.');
		      }
		      
		      if(vm.errors.length > 0){
		    	  return true;
		      }else{
		    	  return false;
		      }
		    },
		    
		limparForm: function(){
			const vm = this;
			vm.Produto_form.id = null,
    		vm.Produto_form.nome = '',
    		vm.Produto_form.fabricante.id = null;
    		vm.Produto_form.volume = '',
    		vm.Produto_form.unidade = '',
    		vm.Produto_form.estoque = ''
    		$('#modal_new').modal('hide');
		},
	    buscarEmTabela: function() {

	    	  var input, filter, table, tr, td, i, txtValue;
	    	  input = $("#txt_consulta");
	    	  filter = input.val();
	    	  table = $("#result_list");
	    	  tr = $(table).find("tbody tr");

	    	  for (i = 0; i < tr.length; i++) {
	    	    td = $(tr[i]).find("td eq:0");
	    	    if (td) {
	    	      txtValue = $(tr[i]).text();
	    	      if (txtValue.indexOf(filter) > -1) {
	    	    	  $(tr[i]).css('display',"");
	    	        $(tr[i]).addClass('d-flex');
	    	      } else {
	    	    	  $(tr[i]).css('display',"none");
	    	        $(tr[i]).removeClass('d-flex');
	    	      }
	    	    } 
	    	  }
	    	}
    }
});