package com.hepta.mercado.rest;

import static org.junit.jupiter.api.Assertions.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.hepta.mercado.entity.Fabricante;

class MercadoFabricanteServiceTest {

	private static WebTarget service;
	private static final String URL_LOCAL = "http://localhost:8080/mercado/rs/fabricantes";
	private static final int ID_FABRICANTE_TEST = 1;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		service = client.target( UriBuilder.fromUri(URL_LOCAL).build() );
	}

	@Test
	void testListaTodosFabricantes() {
		
		Response response = service.request().get();
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}	

	@Test
	void testCreate() {

		Fabricante fabricante = new Fabricante();
		fabricante.setNome("Test JUnit");
	
		Response response = service.request().post(Entity.entity(fabricante, MediaType.APPLICATION_JSON));
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());

	}

    @Test
    public void testUpdate(){
    	
    	
		Fabricante fabricante = new Fabricante();
		fabricante.setId(ID_FABRICANTE_TEST);
		fabricante.setNome("Test JUnit - UPDATED");
		
        Response response = service.path(String.valueOf(ID_FABRICANTE_TEST)).request().put(Entity.entity(fabricante, MediaType.APPLICATION_JSON));
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
    }

    @Test
    public void testDelete(){
    	
        Response response = service.path(String.valueOf(ID_FABRICANTE_TEST)).request().delete();
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
    }

}
