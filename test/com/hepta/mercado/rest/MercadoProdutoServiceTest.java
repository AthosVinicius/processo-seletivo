package com.hepta.mercado.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;

class MercadoProdutoServiceTest {

	private static WebTarget service;
	private static final String URL_LOCAL = "http://localhost:8080/mercado/rs/produtos";
	private static final int ID_PODUTO_TEST = 1;
	private static final int ID_FABRICANTE_TEST = 1;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		service = client.target(UriBuilder.fromUri(URL_LOCAL).build());
	}

	@Test
	void testListaTodosProdutos() {

		Response response = service.request().get();
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

	@Test
	void testCreate() {

		Fabricante fabricante = new Fabricante();
		fabricante.setId(ID_FABRICANTE_TEST);

		Produto produto = new Produto();
		produto.setNome("JUnit - Produto");
		produto.setFabricante(fabricante);
		produto.setVolume(0.5);
		produto.setUnidade("caixa");
		produto.setEstoque(10);

		Response response = service.request().post(Entity.entity(produto, MediaType.APPLICATION_JSON));
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

	@Test
	public void testUpdate() {

		Fabricante fabricante = new Fabricante();
		fabricante.setId(ID_FABRICANTE_TEST);

		Produto produto = new Produto();
		produto.setId(ID_PODUTO_TEST);
		produto.setNome("JUnit - Produto - UPDATED");
		produto.setFabricante(fabricante);
		produto.setEstoque(10);
		produto.setVolume(0.5);
		produto.setUnidade("caixa");

		Response response = service.path(String.valueOf(ID_PODUTO_TEST)).request().put(Entity.entity(produto, MediaType.APPLICATION_JSON));
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

	@Test
	public void testDelete() {

		Response response = service.path(String.valueOf(ID_PODUTO_TEST)).request().delete();
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

}
