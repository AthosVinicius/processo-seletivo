A licen�a MIT (MIT)

Copyright (c) [ano] [nome completo]

� concedida permiss�o, gratuitamente, a qualquer pessoa que obtenha uma c�pia do
este software e os arquivos de documenta��o associados (o "Software"), para lidar
Software sem restri��es, incluindo, sem limita��o, os direitos de
usar, copiar, modificar, mesclar, publicar, distribuir, sublicenciar e / ou vender c�pias de
o Software e para permitir que pessoas a quem o Software seja fornecido fa�am isso,
sujeito �s seguintes condi��es:

O aviso de copyright acima e este aviso de permiss�o devem ser inclu�dos em todos
c�pias ou partes substanciais do Software.

O SOFTWARE � FORNECIDO "COMO EST�", SEM GARANTIA DE QUALQUER TIPO, EXPRESSA OU
IMPL�CITA, INCLUINDO, MAS N�O SE LIMITANDO �S GARANTIAS DE COMERCIALIZA��O, ADEQUA��O
PARA UM FIM ESPEC�FICO E N�O VIOLA��O. EM NENHUM CASO, OS AUTORES OU
OS TITULARES DE DIREITOS AUTORAIS SER�O RESPONSABILIZADOS POR QUALQUER REIVINDICA��O, DANOS OU OUTRAS RESPONSABILIDADES
EM AC��O DE CONTRATO, DELITO OU DE OUTRA FORMA, DECORRENTE DE, FORA OU EM
CONEX�O COM O SOFTWARE OU O USO OU OUTRAS CONCESS�ES NO SOFTWARE.